Favor Task 

-Login and sign up page: handle input validation and save the data on device for this challenge.
-Home page: choose any free/paid images API to load a list of images vertically and handle clicks on each.
-Use Room to save the loaded images offline. The app should be able to work if the connection was lost.
-Optional: create a page where you can select a video from gallery and compress/resize/re-encode it, if the size of the video is bigger than 10MB

