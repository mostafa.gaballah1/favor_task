package favor.sample.task.data.network

import io.reactivex.Observable
import favor.sample.task.data.model.FoodDto
import retrofit2.http.GET

interface ApiService {

    @GET("api/")
    fun getHome(
    ): Observable<FoodDto>


}