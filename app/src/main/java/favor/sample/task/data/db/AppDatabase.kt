package favor.sample.task.data.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import favor.sample.task.data.model.Food
import favor.sample.task.data.model.User

@Database(entities = [Food::class, User::class], version = AppDatabase.VERSION)
abstract class AppDatabase : RoomDatabase() {
    companion object {
        const val DB_NAME = "food.db"
        const val VERSION = 1
    }

    abstract fun foodDao(): FoodDao
    abstract fun authDao(): AuthDao
}