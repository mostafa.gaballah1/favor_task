package favor.sample.task.data.repository

import android.arch.lifecycle.LiveData
import io.reactivex.disposables.Disposable
import favor.sample.task.data.model.Food
import favor.sample.task.data.model.FoodDto
import favor.sample.task.data.network.ApiError

interface AppRepository {

    fun getFoods(
        success: (FoodDto) -> Unit,
        failure: (ApiError) -> Unit = {},
        terminate: () -> Unit = {}
    ): Disposable

    fun insertFood(food: Food): Disposable
    fun getLocalFoods() : LiveData<List<Food>>
}