package favor.sample.task.data.db

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import favor.sample.task.data.model.Food

@Dao
interface FoodDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertFood(food: Food): Long

    @Delete
    fun deleteFood(food: Food): Int

    @Query("SELECT * from Food")
    fun selectAllFoods(): LiveData<List<Food>>

}