package favor.sample.task.data.db

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import favor.sample.task.data.model.Food
import favor.sample.task.data.model.User

@Dao
interface AuthDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: User): Long

    @Query("select * from user where email=:email")
    fun checkEmailNotExist(email: String): LiveData<User>

    @Query("select * from user where email=:email and password=:password")
    fun checkUserExist(email: String, password: String): LiveData<User>
}