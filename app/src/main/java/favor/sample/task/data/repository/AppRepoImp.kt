package favor.sample.task.data.repository

import android.arch.lifecycle.LiveData
import android.util.Log
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import favor.sample.task.data.db.AppDatabase
import favor.sample.task.data.model.Food
import favor.sample.task.data.model.FoodDto
import favor.sample.task.data.network.ApiDisposable
import favor.sample.task.data.network.ApiError
import favor.sample.task.data.network.ApiService

class AppRepoImp(
    val apiService: ApiService,
    val database: AppDatabase
) : AppRepository {

    private val TAG = AppRepoImp::class.java.simpleName
    override fun getFoods(success: (FoodDto) -> Unit, failure: (ApiError) -> Unit, terminate: () -> Unit): Disposable {
        return apiService
            .getHome()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnTerminate(terminate)
            .subscribeWith(
                ApiDisposable<FoodDto>(
                    {

                        success(it)
                    },
                    failure
                )
            )
    }

    override fun insertFood(food: Food) : Disposable =
        Observable
            .fromCallable { database.foodDao().insertFood(food) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                Log.d(TAG, "food added: subscribe: $it")
            }


    override fun getLocalFoods(): LiveData<List<Food>> {
        return database.foodDao().selectAllFoods()
    }
}