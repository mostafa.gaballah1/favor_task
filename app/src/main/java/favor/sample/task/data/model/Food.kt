package favor.sample.task.data.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
@Entity(indices=[ Index(value = ["id"], unique = true) ])
data class Food (
    @PrimaryKey(autoGenerate = true)
    @SerializedName("id") var id: Int,
    @SerializedName("title") var title: String,
    @SerializedName("href") var href: String,
    @SerializedName("ingredients") var ingredients: String,
    @SerializedName("thumbnail") var thumbnail: String
): Parcelable