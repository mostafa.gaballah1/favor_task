package favor.sample.task.ui

import android.content.Context
import android.content.Intent
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import favor.sample.task.BR.item
import favor.sample.task.data.model.Food
import favor.sample.task.databinding.FoodItemRowBinding
import favor.sample.task.ui.base.DataBindingViewHolder
import favor.sample.task.ui.details.DetailsActivity

class HomeAdapter(
    private var items: MutableList<Food> = arrayListOf(), private val context: Context
) : RecyclerView.Adapter<HomeAdapter.SimpleVideoHolder>() {
    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: SimpleVideoHolder, position: Int) {
        holder.onBind(items[position])
        holder.dataBinding.root.setOnClickListener {
            var intent = Intent(context, DetailsActivity::class.java)
            intent.putExtra("food", items[position])
            context.startActivity(intent)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SimpleVideoHolder {
        val binding  = FoodItemRowBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return SimpleVideoHolder(binding)
    }

    inner class SimpleVideoHolder(dataBinding: ViewDataBinding)
        : DataBindingViewHolder<Food>(dataBinding)  {
        override fun onBind(t: Food): Unit = with(t) {
            dataBinding.setVariable(item,t)
        }
    }

    fun add(list: List<Food>) {
        items.addAll(list)
        notifyDataSetChanged()
    }

    fun clear() {
        items.clear()
        notifyDataSetChanged()
    }
}