package favor.sample.task.ui.register

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import dagger.android.support.DaggerAppCompatActivity
import favor.sample.task.R
import favor.sample.task.data.model.User
import favor.sample.task.ui.MainActivity
import favor.sample.task.util.SharedPrefsUtil
import kotlinx.android.synthetic.main.activity_registration.*
import javax.inject.Inject


class RegistrationActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: RegistrationViewModel by lazy { ViewModelProviders.of(this,viewModelFactory).get(
        RegistrationViewModel::class.java) }

    private var isRegistered: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

        signup_btn.setOnClickListener {
            checkSignup()
        }
    }

    private fun checkSignup() {
        if(TextUtils.isEmpty(name_et.text.toString())) {
            name_et.error = "Enter your name"
        } else if(TextUtils.isEmpty(email_et.text.toString())) {
            email_et.error = "Enter your email"
        } else if(!isEmailValid(email_et.text.toString())) {
            email_et.error = "Enter valid email"
        } else if(TextUtils.isEmpty(password_et.text.toString())) {
            password_et.error = "Enter your password"
        } else if(TextUtils.isEmpty(confirm_password_et.text.toString())) {
            confirm_password_et.error = "Enter your confirm password"
        } else if(!password_et.text.toString().equals(confirm_password_et.text.toString())) {
            Toast.makeText(this,"Password and confirm password not match!", Toast.LENGTH_LONG).show()

        } else {
            checkInputEmail()
        }
    }

    private fun isEmailValid(email: String): Boolean {
        val regex = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$"
        return regex.toRegex().matches(email)
    }

    private fun checkInputEmail() {
        viewModel.checkEmailNotExist(email_et.text.toString())
            .observe(this, Observer { user->
                if(!isRegistered) {
                    if (user != null) {
                        Toast.makeText(
                            this,
                            "This email exist before, please enter another email",
                            Toast.LENGTH_LONG
                        ).show()
                    } else {
                        isRegistered = true
                        registerUser()
                    }
                }
            })
    }

    private fun registerUser() {
        val user = User()
        user.name = name_et.text.toString()
        user.email = email_et.text.toString()
        user.password = password_et.text.toString()
        viewModel.insertUser(user).observe(this, Observer {
            if(it!!) {
                SharedPrefsUtil.saveBool(this, "IS_LOGIN", true)
                finishAffinity()
                startActivity(Intent(this, MainActivity::class.java))
            } else {
                Toast.makeText(this,"Error occured", Toast.LENGTH_LONG).show()
            }
        })
    }
}
