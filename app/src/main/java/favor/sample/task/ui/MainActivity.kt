package favor.sample.task.ui


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import android.view.View
import android.widget.Toast
import dagger.android.support.DaggerAppCompatActivity
import favor.sample.task.R
import favor.sample.task.data.model.Food
import favor.sample.task.data.model.FoodDto
import favor.sample.task.ui.login.LoginActivity
import favor.sample.task.util.NetworkUtil
import favor.sample.task.util.SharedPrefsUtil
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar_main.*
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: HomeViewModel by lazy { ViewModelProviders.of(this,viewModelFactory).get(HomeViewModel::class.java) }
    val adapter : HomeAdapter by lazy { HomeAdapter(arrayListOf(), this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar: Toolbar by lazy { toolbar_main_activity }
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayShowTitleEnabled(false)
        checkCache()
        observeViewModel()

        log_out_iv.setOnClickListener {
            SharedPrefsUtil.saveBool(this, "IS_LOGIN", false)
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }
    }

    private fun checkCache() {
        if(!NetworkUtil.isNetworkConnected(this)) {
            if(!SharedPrefsUtil.getBool(this, "SAVED_LOCALLY", false)) {
                Toast.makeText(this, "No internet connection", Toast.LENGTH_LONG).show()
            } else {
                getLocalData()
            }
        } else {
            viewModel.getFoods()
        }

    }

    private fun getLocalData() {
        viewModel.getLocalFoods().observe(this, Observer {
            initView(it)
        })
    }

    private fun observeViewModel() {
        with(viewModel) {
            homeData.observe(this@MainActivity, Observer {
                initView(it?.results)
                if(it != null && it.results.size > 0) {
                    SharedPrefsUtil.saveBool(this@MainActivity, "SAVED_LOCALLY", true)
                }
            }
            )
            error.observe(this@MainActivity, Observer {
                progressBar_home.visibility= View.GONE
                Toast.makeText(this@MainActivity, "${it?.message}", Toast.LENGTH_LONG).show()
            })
        }
    }

    private fun initView(foodList: List<Food>?) {
        rv_main_home.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        rv_main_home.adapter = adapter
        progressBar_home.visibility= View.GONE
        if (foodList!!.isNotEmpty()) {
            adapter.clear()
            adapter.add(foodList)

        }else{
            Toast.makeText(this, getString(R.string.empty_list), android.widget.Toast.LENGTH_LONG).show()
        }
    }


}
