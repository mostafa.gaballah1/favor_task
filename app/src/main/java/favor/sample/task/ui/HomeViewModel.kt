package favor.sample.task.ui

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import favor.sample.task.data.model.Food
import favor.sample.task.data.model.FoodDto
import favor.sample.task.data.network.ApiError
import favor.sample.task.data.repository.AppRepository
import favor.sample.task.ui.base.BaseViewModel
import favor.sample.task.util.SharedPrefsUtil
import javax.inject.Inject

class HomeViewModel @Inject constructor(val appRepository: AppRepository) : BaseViewModel() {
    private val TAG = HomeViewModel::class.java.simpleName
    val homeData: MutableLiveData<FoodDto> by lazy { MutableLiveData<FoodDto>() }
    val error : MutableLiveData<ApiError> by lazy { MutableLiveData<ApiError>() }

    fun getFoods() {
        appRepository.getFoods(

            { foodDto ->
                Log.d(TAG, "getHomeData.success() called with: $foodDto")
                homeData.postValue(foodDto)
                if(foodDto.results.size>0){
                    var id = 1
                    for (food in foodDto.results){
                        food.id = id
                        appRepository.insertFood(food).also { compositeDisposable.add(it) }
                        ++id
                    }
                }
            },
            {
                Log.d(TAG, "getHomeData.error() called with: $it")
                error.value = it
            },
            {

            }
        ).also { compositeDisposable.add(it) }
    }

    fun getLocalFoods() : LiveData<List<Food>> {
        return appRepository.getLocalFoods()
    }


}