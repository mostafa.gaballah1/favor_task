package favor.sample.task.ui.login

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.widget.Toast
import dagger.android.support.DaggerAppCompatActivity
import favor.sample.task.R
import favor.sample.task.ui.MainActivity
import favor.sample.task.ui.register.RegistrationActivity
import favor.sample.task.util.SharedPrefsUtil
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

class LoginActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel: LoginViewModel by lazy { ViewModelProviders.of(this,viewModelFactory).get(
        LoginViewModel::class.java) }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        checkIsLogin()
        login_btn.setOnClickListener {
            checkLogin()
        }

        register_tv.setOnClickListener {
            startActivity(Intent(this, RegistrationActivity::class.java))
        }
    }

    private fun checkIsLogin() {
        if(SharedPrefsUtil.getBool(this, "IS_LOGIN", false)) {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }
    }

    private fun checkLogin() {
        if(TextUtils.isEmpty(email_et.text.toString())) {
            email_et.error = "Enter your email"
        } else if(TextUtils.isEmpty(password_et.text.toString())) {
            password_et.error = "Enter your password"
        } else {
            viewModel.login(email_et.text.toString(), password_et.text.toString())
                .observe(this, Observer {
                    if(it != null) {
                        SharedPrefsUtil.saveBool(this, "IS_LOGIN", true)
                        startActivity(Intent(this, MainActivity::class.java))
                        finish()
                    } else {
                        Toast.makeText(this,"Wrong email or password", Toast.LENGTH_LONG).show()
                    }
                })
        }
    }

}
