package favor.sample.task.ui.details

import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import favor.sample.task.R
import favor.sample.task.data.model.Food
import favor.sample.task.databinding.ActivityDetailsBinding

class DetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val databinding : ActivityDetailsBinding= DataBindingUtil.setContentView(this, R.layout.activity_details)

        val food = intent.getParcelableExtra<Food>("food")
        databinding.item = food
    }
}
