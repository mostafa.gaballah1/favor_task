package favor.sample.task.ui.base

import android.databinding.BindingAdapter
import android.graphics.drawable.Drawable
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import favor.sample.task.R


object CustomBindingAdapter{

    @JvmStatic
    @BindingAdapter("image_url", "image_progressbar", requireAll = false)
    fun loadImage(imageView: ImageView, url: String, image_progressbar: ProgressBar) {
        Glide.with(imageView.context)
            .load(url)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .listener(object : RequestListener<Drawable>{
                override fun onLoadFailed(e: GlideException?, model: Any?,
                                          target: Target<Drawable>?, isFirstResource: Boolean): Boolean {
                    image_progressbar.visibility = View.GONE
                    Glide.with(imageView.context)
                        .load(R.drawable.image_placeholder).into(imageView)
                    return false
                }

                override fun onResourceReady(resource: Drawable?, model: Any?, target: Target<Drawable>?,
                                             dataSource: com.bumptech.glide.load.DataSource?, isFirstResource: Boolean): Boolean {
                    image_progressbar.visibility = View.GONE
                    return false
                }
            })
            .into(imageView)
    }

}