package favor.sample.task.ui.login

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import favor.sample.task.data.db.AuthDao
import favor.sample.task.data.model.User
import javax.inject.Inject

class LoginViewModel @Inject constructor(var authDao: AuthDao) : ViewModel() {



    public fun login(email : String , password : String) : LiveData<User>
            = authDao.checkUserExist(email, password)
}