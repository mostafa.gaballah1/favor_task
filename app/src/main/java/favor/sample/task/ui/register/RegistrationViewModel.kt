package favor.sample.task.ui.register

import android.annotation.SuppressLint
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import favor.sample.task.data.db.AuthDao
import favor.sample.task.data.model.User
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class RegistrationViewModel @Inject constructor(var authDao: AuthDao) : ViewModel() {

    fun checkEmailNotExist(email : String) : LiveData<User>  =
        authDao.checkEmailNotExist(email)

    @SuppressLint("CheckResult")
    fun insertUser(user : User) : LiveData<Boolean> {

        var registerLiveData : MutableLiveData<Boolean> = MutableLiveData()

        Observable
            .fromCallable { authDao.insertUser(user) }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe ({
                registerLiveData.value = true
            }, {
                registerLiveData.value = false
            })

        return registerLiveData
    }
}