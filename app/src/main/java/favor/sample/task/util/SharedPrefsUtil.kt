package favor.sample.task.util

import android.content.Context
import android.content.SharedPreferences


class SharedPrefsUtil {

    companion object{
    fun saveBool(context: Context, key: String, value: Boolean) {
        if (context != null)
            getSharedPreference(context).edit().putBoolean(key, value).apply()
    }

    fun getBool(context: Context, key: String, defValue: Boolean): Boolean {
        return getSharedPreference(context).getBoolean(key, defValue)
    }

    private fun getSharedPreference(context: Context): SharedPreferences {
        return context.getSharedPreferences("PREFS", Context.MODE_PRIVATE)
    }
    }
}