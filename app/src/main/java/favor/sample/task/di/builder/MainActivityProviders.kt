package favor.sample.task.di.builder

import dagger.Module
import dagger.android.ContributesAndroidInjector
import favor.sample.task.ui.HomeFragment

@Module
abstract class MainActivityProviders{
    @ContributesAndroidInjector
    abstract fun provideHomeFragment(): HomeFragment

}