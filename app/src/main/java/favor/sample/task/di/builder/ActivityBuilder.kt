package favor.sample.task.di.builder

import dagger.Module
import dagger.android.ContributesAndroidInjector
import favor.sample.task.ui.MainActivity
import favor.sample.task.ui.login.LoginActivity
import favor.sample.task.ui.register.RegistrationActivity

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun bindLoginActivity(): LoginActivity

    @ContributesAndroidInjector
    abstract fun bindRegistrationActivity(): RegistrationActivity
}